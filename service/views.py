from rest_framework import viewsets

from service.models import Category, Service
from service.serializers import CategoryListSerializer, CategoryDetailSerializer, ServiceSerializer


class CategoryViewSet(viewsets.ModelViewSet):
    def get_serializer_class(self):
        if self.action == 'retrieve':
            return CategoryDetailSerializer
        return CategoryListSerializer

    queryset = Category.objects.all()
    lookup_field = 'slug'


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    lookup_field = 'slug'
