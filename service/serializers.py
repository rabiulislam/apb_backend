from rest_framework import serializers

from service.models import Category, Service, Banner


class ServiceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Service
        fields = '__all__'
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'},
            'category': {'lookup_field': 'slug'},
        }


class BannerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Banner
        fields = ['title', 'path', 'description', 'image']


class CategoryListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(
        view_name='category-detail',
        lookup_field='slug',
    )

    class Meta:
        model = Category
        fields = ['name', 'slug', 'parent', 'url']
        lookup_field = 'slug'


class CategoryDetailSerializer(serializers.ModelSerializer):
    banner = BannerSerializer(read_only=True)
    services = ServiceSerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['name', 'slug', 'parent', 'banner', 'services']
        lookup_field = 'slug'
