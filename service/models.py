from django.core.validators import RegexValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.fields import TreeForeignKey
from mptt.models import MPTTModel

phone_regex = RegexValidator(regex=r'^(?:\+88|88)?(0(1|9)[3-9]\d{7,10})$',
                             message=_("Must be a valid phone number of bangladesh"))


class WordChoice:
    FOUR = 4
    FIVE = 5
    SIX = 6

    CHOICES = (
        (FOUR, "Four"),
        (FIVE, "Five"),
        (SIX, "Six"),
    )


class Banner(models.Model):
    title = models.CharField(max_length=300)
    description = models.TextField(null=True, blank=True)
    path = models.URLField(null=True, blank=True)
    image = models.ImageField(upload_to='banners/')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Category(MPTTModel):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, unique=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    banner = models.ForeignKey("Banner", null=True, blank=True, on_delete=models.PROTECT, related_name='+')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name_plural = 'Categories'


class Service(models.Model):
    name = models.CharField(max_length=300)
    slug = models.SlugField(max_length=300)
    category = models.ForeignKey("Category", related_name='services', on_delete=models.PROTECT)

    description = models.TextField(null=True, blank=True)
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)

    area = models.CharField(max_length=200)
    word = models.SmallIntegerField(choices=WordChoice.CHOICES)

    website_url = models.URLField(max_length=300, null=True, blank=True)
    facebook_url = models.URLField(max_length=300, null=True, blank=True)
    twitter_url = models.URLField(max_length=300, null=True, blank=True)
    youtube_url = models.URLField(max_length=300, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
